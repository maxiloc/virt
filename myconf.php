<?php

require('conf.php');
require('host.php');

$hosts = getHosts();

function completion()
{
	return array("list", "add", "print", "delete", "export", "exit", "update");
}
readline_completion_function('completion');

function printHelp()
{
	echo "\033[0;37m─────── VirtualHost Manager ───────\n\033[0;33m";
	echo "list [num]\n";
	echo "print [num]\n";
	echo "add\n";
	echo "update num\n";
	echo "delete [num]\n";
	echo "export\n";
	echo "exit\n";
	echo "\033[0;37m───────────────────────────────────\n\033[0;33m";
}

function error($error)
{
	echo "\033[0;35m";
	echo $error;
	echo "\033[0;33m";
}

printHelp();

while(true)
{
	$line = readline("$ ");
	if (!empty($line))
		readline_add_history($line);
	$line = explode(" ", $line);
	if ($line[0] == "print")
	{
		if (count($line) == 1)
			printHosts($hosts);
		else
		{
			 if (count($line) == 2)
			 {
				if (is_numeric($line[1]))
				{
					if ($line[1] > 0 && $line[1] <= count($hosts))
						echo $hosts[$line[1] - 1]->toString();
					else
						error("Error => bad id\n");
				}
				else
					error("Usage: $ print [num]\n");
			}
			else
				error("Usage: $ print [num]\n");
		}
	}
	else if ($line[0] == "list")
	{
		if (count($line) == 1)
			listHosts($hosts);
		else
		{
			 if (count($line) == 2)
			 {
				if (is_numeric($line[1]))
				{
					if ($line[1] > 0 && $line[1] <= count($hosts))
						listHost($hosts[$line[1] - 1], $line[1]);
					else
						error("Error => bad id\n");
				}
				else
					error("Usage: $ list [num]\n");
			}
			else
				error("Usage: $ list [num]\n");
		}
	}
	else if ($line[0] == "update")
	{
		echo "\n\033[0;37m───────────────────────────────────\n";
		echo "────── Update a VirtualHost ───────\n";
		echo "───────────────────────────────────\n\n\033[0;33m";

		if (count($line) != 2 || is_numeric($line[1]) == false)
		{
			error("Usage: $ update num\n");
			continue;
		}
		if ($line[1] < 0 || $line[1] > count($hosts))
		{
			error("Error => bad id");
			continue;
		}
		$current = $hosts[$line[1] - 1];
		$address = "";
		while (true)
		{
			$b = true;
			readline_add_history($current->address);
			echo "current address is : \"".$current->address."\" press enter to keep the same\n";
			$address = readline("Enter the adress $ ");
			foreach ($hosts as $host)
				if ($host->address == $address && $host != $current)
					$b = false;
			if ($b && $address != "127.0.0.1")
			{
				if ($address != "")
					$current->address = $address;
				break;
			}
			if ($address == "*:80")
				break;
			if ($address == "127.0.0.1")
				error("Error => 127.0.0.1 is your localhost\n");
			else
				error("Error => address already used\n");
		}
		readline_add_history($current->path);
		echo "current path is : \"".$current->path."\" press enter to keep the same\n";
		$path = readline("Enter the path to application $ ");
		if ($path != "")
			$current->path = $path;
		readline_add_history($current->name);
		echo "current name is : \"".$current->name."\" press enter to keep the same\n";
		$name = readline("Enter the name of the application $ ");
		if ($name != "")
			$current->name = $name;
		$hosts[$line[1] - 1] = $current;
	}
	else if ($line[0] == "add")
	{
		echo "\n\033[0;37m───────────────────────────────────\n";
		echo "──────── Add a VirtualHost ────────\n";
		echo "───────────────────────────────────\n\n\033[0;33m";

		$address = "";
		while (true)
		{
			$b = true;
			readline_add_history("127.0.0.");
			$address = readline("Enter the adress $ ");
			foreach ($hosts as $host)
				if ($host->address == $address)
					$b = false;
			if ($b && $address != "127.0.0.1")
				break;
			if ($address[0] == "*")
				break;
			if ($address == "127.0.0.1")
				error("Error => 127.0.0.1 is your localhost\n");
			else
				error("Error => address already used\n");
		}
		$path = "";
		while ($path == "")
			$path = readline("Enter the path to application $ ");
		$name = "";
		while ($name == "")
			$name = readline("Enter the name of the application $ ");
		$host = new Host($address, $path, $path, $name);
		listHost($host, count($hosts) + 1);
		$res = readline("Are you sure to add this VirtualHost [y/n] $ ");
		if ($res == "y")
		{
			$hosts[] = $host;
		}
	}
	else if ($line[0] == "export")
	{
		$export = "";
		foreach ($hosts as $host)
			$export .= $host->toString();
		$fp = fopen(CONF_FILE, "w+");
		fwrite($fp, "NameVirtualHost *:80\n\n");
		fwrite($fp, $export);
		fclose($fp);
	}
	else if ($line[0] == "delete")
	{
		 if (count($line) == 2)
		 {
			if (is_numeric($line[1]))
			{
				if ($line[1] > 0 && $line[1] <= count($hosts))
				{
					unset($hosts[$line[1] - 1]);
					$hosts = array_values($hosts);
				}
				else
					echo "Error => bad id\n";
			}
			else
				echo "Usage: $ delete [num]\n";
		}
		else
			echo "Usage: $ delete [num]\n";

	}
	else if ($line[0] == "exit")
	{
		break;
	}
	else
	{
		printHelp();
	}
}
