<?php


class Host
{
	var $address;
	var $path;
	var $name;
	var $valid;

	function __construct($address, $path, $path2, $name)
	{
		$this->address = $address;
		$this->path = $path;
		$this->name = $name;
		$this->valid = $path == $path2;
	}

	function toString()
	{
		$s  = "<VirtualHost ".$this->address.">\n";
		$s .= "  DocumentRoot \"".$this->path."\"\n";
		$s .= "  ServerName ".$this->name."\n";
		$s .= "  <Directory \"".$this->path."\">\n";
		$s .= "    Order allow,deny\n";
		$s .= "    Allow from all\n";
		$s .= "    AllowOverride All\n";
		$s .= "  </Directory>\n";
		$s .= "</VirtualHost>\n\n";

		return $s;
	}

	public static function printTabLine($c1, $c2, $c3, $c4, $refs)
	{
		echo getCollumn($c1, $refs[0]);
		echo getCollumn($c2, $refs[1]);
		echo getCollumn($c3, $refs[2]);
		echo getCollumn($c4, $refs[3]);
		echo "  \n";
	}

	function printLine($i, $refs)
	{
		Host::printTabLine($i, $this->address, $this->path, $this->name, $refs);
	}
}

function getHosts()
{
	$content = file_get_contents(CONF_FILE);

	$results = array();
	preg_match_all("/<VirtualHost\ (.*?)>.*?DocumentRoot\ \"(.*?)\".*?ServerName (.*?)\n.*?<Directory \"(.*?)\">.*?<\/Directory>.*?<\/VirtualHost>/s", $content, $results);

	$hosts = array();

	for ($i = 0; $i < count($results[0]); $i++)
	{
		$host = new Host($results[1][$i], $results[2][$i], $results[4][$i], $results[3][$i]);
		$hosts[] = $host;
	}
	return $hosts;
}

function printHosts($hosts)
{
	foreach ($hosts as $host)
	{
		echo $host->toString();
	}
}

function repeat($c, $width)
{
	for ($ii = 0; $ii < $width; $ii++)
		echo $c;
	echo "\n";
}

function getCollumn($content, $maxsize)
{
	$s = "  ".$content;
	$diff = $maxsize - strlen($content) + 1;
	for ($i = 0; $i < $diff; $i++)
		$s .= " ";
	return $s;
}

function getRefs()
{
	$refs = array();
	$refs[] = strlen("");
	$refs[] = strlen("Address");
	$refs[] = strlen("Path");
	$refs[] = strlen("Name");

	return $refs;
}

function listHost($host, $i)
{
	$refs = getRefs();
	$host->printLine($i, $refs);
}
function listHosts($hosts)
{
	$refs = getRefs();
	$i = 1;
	$max0 = 0;
	$max1 = $refs[1];
	$max2 = $refs[2];
	$max3 = $refs[3];

	foreach ($hosts as $host)
	{
		$max0 = max(strlen($i), $max0);
		$max1 = max(strlen($host->address), $max1);
		$max2 = max(strlen($host->path), $max2);
		$max3 = max(strlen($host->name), $max3);
		$i++;
	}

	$refs[0] = $max0;
	$refs[1] = $max1;
	$refs[2] = $max2;
	$refs[3] = $max3;

	$width = $max1 + $max2 + $max3 + 15;

	$i = 1;
	repeat('─', $width);
	Host::printTabLine(" ", "Address", "Path", "Name", $refs);
	repeat('─', $width);
	foreach ($hosts as $host)
	{
		$host->printLine($i, $refs);
		$i++;
	}
	repeat("─", $width);
}
